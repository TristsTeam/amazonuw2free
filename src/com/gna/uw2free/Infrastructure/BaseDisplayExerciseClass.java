package com.gna.uw2free.Infrastructure;

import com.gna.uw2free.R;

import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class BaseDisplayExerciseClass extends CommonBaseClass {
	protected String[] exerciseTextArray;
	protected String[] exerciseNameArray;
	protected int[] firstPictureArray;
	protected int[] secondPictureArray;
	protected int[] thirdPictureArray;
	protected int[] imageCount;

	protected ImageView firstImage;
	protected ImageView secondImage;
	protected ImageButton rightArrowBtn;
	protected ImageButton leftArrowBtn;
	protected ImageButton cancelBtn;
	protected TextView exerciseText;
	protected TextView exerciseCount;
	protected TextView exerciseName;
	protected ImageView titleView;
	protected ImageButton infoBtn;

	public void intializeViews() {
		firstImage = (ImageView) findViewById(R.id.firstImage);
		secondImage = (ImageView) findViewById(R.id.secondImage);
		rightArrowBtn = (ImageButton) findViewById(R.id.rightArrowBtn);
		leftArrowBtn = (ImageButton) findViewById(R.id.leftArrowBtn);
		exerciseText = (TextView) findViewById(R.id.exerciseText);
		exerciseCount = (TextView) findViewById(R.id.exerciseCount);
		exerciseName = (TextView) findViewById(R.id.exerciseName);
		titleView = (ImageView) findViewById(R.id.titleView);
		cancelBtn = (ImageButton) findViewById(R.id.cancelBtn);
		infoBtn = (ImageButton) findViewById(R.id.infoBtn);

	}

	public void workoutAExercise() {
		titleView.setBackgroundResource(R.drawable.title01);
		exerciseTextArray = new String[7];
		exerciseNameArray = new String[7];
		firstPictureArray = new int[7];
		secondPictureArray = new int[7];
		thirdPictureArray = new int[7];
		imageCount = new int[7];

		exerciseTextArray[0] = "In a squat position, squat down legs parallel, then explode up and jump, land softly and repeat.";
		exerciseTextArray[1] = "In a push up position, bring your left knee under and across your body. Hold. Repeat the other side.";
		exerciseTextArray[2] = "Holding two dumbbells at a 90 degree frontal position, tense your core tight and begin to squat down and then explode upward as you press the DBs upward.";
		exerciseTextArray[3] = "Bend at your wait with a flat back and knees bent slightly.Flye your DBs outward and back, pause return and repeat";
		exerciseTextArray[4] = "Holding two dumbbells in a push up position, brace your core so you do not dip or arch your hips, begin to row alternately each dumbbell to your side, pausing then return and repeat.";
		exerciseTextArray[5] = "Laying on your back, hold two dumbbells outward in a flye position, , then lift your feet together. holding the legs almost vertical as you begin to bring the weights together with straight arms. Repeat.";
		exerciseTextArray[6] = "Holding 1 dumbbell, and in a laying position with knees bent.  Crunch your abs as you raise the dumbbell upward, pause, return and repeat.";

		exerciseNameArray[0] = "BW Squat Jumps";
		exerciseNameArray[1] = "Knee Unders";
		exerciseNameArray[2] = "Front Thruster";
		exerciseNameArray[3] = "Bent Reverse DB Flye";
		exerciseNameArray[4] = "Renegade Row";
		exerciseNameArray[5] = "Sky Pointer Flyes";
		exerciseNameArray[6] = "DB Finger Point Crunch";

		firstPictureArray[0] = R.drawable.w1_exercise01_01;
		firstPictureArray[1] = R.drawable.w1_exercise02_01;
		firstPictureArray[2] = R.drawable.w1_exercise03_01;
		firstPictureArray[3] = R.drawable.w1_exercise04_01;
		firstPictureArray[4] = R.drawable.w1_exercise05_01;
		firstPictureArray[5] = R.drawable.w1_exercise06_01;
		firstPictureArray[6] = R.drawable.w1_exercise07_01;

		secondPictureArray[0] = R.drawable.w1_exercise01_02;
		secondPictureArray[1] = R.drawable.w1_exercise02_02;
		secondPictureArray[2] = R.drawable.w1_exercise03_02;
		secondPictureArray[3] = R.drawable.w1_exercise04_02;
		secondPictureArray[4] = R.drawable.w1_exercise05_02;
		secondPictureArray[5] = R.drawable.w1_exercise06_02;
		secondPictureArray[6] = R.drawable.w1_exercise07_02;

		thirdPictureArray[0] = 0;
		thirdPictureArray[1] = 0;
		thirdPictureArray[2] = 0;
		thirdPictureArray[3] = 0;
		thirdPictureArray[4] = 0;
		thirdPictureArray[5] = 0;
		thirdPictureArray[6] = 0;

		imageCount[0] = 2;
		imageCount[1] = 2;
		imageCount[2] = 2;
		imageCount[3] = 2;
		imageCount[4] = 2;
		imageCount[5] = 2;
		imageCount[6] = 2;

	}
}
